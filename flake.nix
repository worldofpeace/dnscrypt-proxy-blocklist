{
  description = "worldofpeace's blocklist for dnscrypt-proxy (generated using generate domains-blocklist)";

   inputs = {
      nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
      flake-utils.url = "github:numtide/flake-utils";
      flake-compat = {
        url = "github:edolstra/flake-compat";
        flake = false;
      };
    };

  outputs = { self, nixpkgs, flake-utils, ... }:
   flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system}; in
      rec {
        packages.dnscrypt-proxy-blocklist = pkgs.callPackage ./package.nix {};
        defaultPackage = packages.dnscrypt-proxy-blocklist;
      });
}
