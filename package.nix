{ unzip
, stdenv
, fetchurl
}:

let
  metadata = stdenv.lib.importJSON ./sources.json;
in

stdenv.mkDerivation rec {
  name = "dnscrypt-proxy-blocklist";

  src = fetchurl {
    name = "archive.zip";
    inherit (metadata) url sha256;
  };

  unpackPhase = ''
    unzip $src
  '';

  nativeBuildInputs = [
    unzip
  ];

  dontBuild = true;
  dontConfigure = true;

  installPhase = ''
    mkdir -p $out/etc/dnscrypt-proxy
    mv blocklist/blocklist.txt $out/etc/dnscrypt-proxy
  '';
}
