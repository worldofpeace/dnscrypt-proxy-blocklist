#!/usr/bin/env nix-shell
#! nix-shell -p curl -i bash

set -e

#
# ─── SET VARS IN PROJECT SETTINGS ───────────────────────────────────────────────
#

    function setup_env ( ) {
        BASE_URL=${BASE_URL:-$(echo $CI_PROJECT_URL | cut -d'/' -f1-3)}
        VARS=( BASE_URL PRIVATE_TOKEN PROJECT JOB_NAME )
    }

#
# ─── SOURCE SCRIPT TO CHECK VARS ────────────────────────────────────────────────
#

    function run_check_vars ( ) {
        . ./ci/check-vars.sh

        check_vars
    }

#
# ─── DOWNLOAD THE ARTIFACT ──────────────────────────────────────────────────────
#

    function download_latest_artifact ( ) {
      [ -z $OUT_FILE ] && OUT_FILE="artifacts.zip"
      curl -fksSL -o ${OUT_FILE} -H \
        "PRIVATE-TOKEN: ${PRIVATE_TOKEN}" "${BASE_URL}/api/v4/projects/${PROJECT}/jobs/artifacts/${REF:-master}/download?job=${JOB_NAME}"
    }


#
# ─── MAIN ───────────────────────────────────────────────────────────────────────
#

setup_env
run_check_vars

download_latest_artifact
