#!/usr/bin/env nix-shell
#! nix-shell -p jo nix -i bash

URL=$1
FILE=$2

SHA256=$(nix hash-file --base64 --type sha256 $FILE)

jo url=$URL sha256=$SHA256
