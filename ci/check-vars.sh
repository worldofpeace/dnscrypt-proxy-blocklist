#!/usr/bin/env bash

check_vars() {
  for VAR in ${VARS[@]}
  do
    if [ -z ${!VAR} ]; then echo "\$${VAR} is missing"; exit 1; fi
  done
}
