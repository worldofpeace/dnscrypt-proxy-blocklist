#!/usr/bin/env nix-shell
#! nix-shell -p awscli2 -i bash

set -e

AWS_BUCKET_NAME="$1"
PROJECT_NAME="$2"
FILE="$3"

OBJECT_KEY="$PROJECT_NAME/$FILE"
S3_URI="s3://$AWS_BUCKET_NAME/$OBJECT_KEY"

delete_file() {
    aws s3 rm "$S3_URI"
    exit 1
}

aws s3 cp "$FILE" "$S3_URI"

exit_status=$?
if [[ $exit_status -eq 0 ]]; then
    # Tag it so it can be downloaded publically.
    # AWS S3 is configured that at $PROJECT_NAME tagged can be tagged with public=yes
    aws s3api put-object-tagging \
        --bucket "$AWS_BUCKET_NAME" \
        --key "$OBJECT_KEY" \
        --tagging 'TagSet={Key=public,Value=yes}'

    exit_status=$?
    if ! [[ $exit_status -eq 0 ]]; then
        delete_file
    fi
else
    echo "Failed to upload file $FILE to S3"
    exit 1
fi
